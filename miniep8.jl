#Função responsável pela troca de dois itens (i,j) dentro de um vetor (v)
function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

#Função responsável pela ordenação do vetor de cartas passado (v)
function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

#Função responsável somente por comparar o valor de duas cartas dadas (x, y) sem distinção de naipe.
function compareByValue(x, y)
    cartas = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"]
    tam = length(cartas)
    indexOfX = 1
    indexOfY = 1

    for i in 1:tam
        if occursin(cartas[i], x)
            indexOfX = i
        end
        if occursin(cartas[i], y)
            indexOfY = i
        end
    end
    if indexOfX<indexOfY
        return true
    end
    return false
end

#Função responsável por gerenciar buscarIndex
#e responsável por comparar o valor de duas cartas pelo seu valor e seu naipe
function compareByValueAndSuit(x, y)
    naipe = ["♦","♠","♥","♣"]
    cartas = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"]

    resul = buscarIndex(naipe, x, y)

    if resul == "="
        resul = buscarIndex(cartas, x, y)
        if resul == "="
            return false
        end
        return resul
    else
    return resul
    end
end

#Função que busca o index de dois valores dados (x e y) em determinado vetor (v)
#e retorna true caso indexX < indexY e false caso indexX > indexY e "=" caso o index de x = index de y
function buscarIndex(v, x, y)
    tam = length(v)
    indexOfX = 1
    indexOfY = 1

    for i in 1:tam
        if occursin(v[i], x)
            indexOfX = i
        end
        if occursin(v[i], y)
            indexOfY = i
        end
    end
    if indexOfX < indexOfY
        return true
    elseif indexOfX == indexOfY
        return "="
    end
    return false
end


using Test
#Função de testes
function test()

    # Teste para a função compareByValueAndSuit
    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")
    @test compareByValueAndSuit("A♠", "4♥")
    @test !compareByValueAndSuit("A♠", "3♠")
    print("Teste de compareByValueAndSuit: OK \n")

    # Teste para a função insercao
    @test (insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])) == (["10♦","J♠","K♠","A♠","A♠","10♥"])
    @test (insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠", "2♦"])) == (["2♦", "10♦","J♠","K♠","A♠","A♠","10♥"])
    @test (insercao(["10♦", "K♠", "A♠", "J♠", "A♠"])) == (["10♦","J♠","K♠","A♠","A♠"])
    @test (insercao(["10♦", "K♠","J♠", "A♠"])) == (["10♦","J♠","K♠","A♠"])
    @test (insercao(["A♠"])) == (["A♠"])
    @test (insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])) != (["10♥","10♦","J♠","K♠","A♠","A♠"])
    print("Teste de ordenação: OK \n")
end
